import React from 'react';
import { elephant } from '../WeightsCalculate';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Confetti from 'react-dom-confetti';
import  ResultBox from './ResultBox';

const config = {
    angle: 90,
    spread: "180",
    startVelocity: 45,
    elementCount: 50,
    dragFriction: "0.17",
    duration: "2250",
    stagger: 0,
    width: "10px",
    height: "10px",
    colors: ["#a864fd", "#29cdff", "#78ff44", "#ff718d", "#fdff6a"]
};

export default class InputField extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      isVisible: false,
      isDisabled: false,
      resultValue: null,
      confettiBoom: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.capitalizeString = this.capitalizeString.bind(this);
  }

  handleChange(e) {
    this.setState({
      value: e.target.value,
      confettiBoom: false
    });
  }

  handleClick(e) {
    this.setState({
      isVisible: true,
      isDisabled: !this.state.isDisabled,
      resultValue: elephant.weight(this.state.value),
      confettiBoom: true
    })
    console.log(elephant.weight(this.state.value));
  }

  capitalizeString(str) {
    if (!str) return str;

    return str[0].toUpperCase() + str.slice(1);
  }

  render() {
    return (
      <>
        <div className="inputs-container">
          <TextField
            className="inputfield"
            value={this.state.value}
            onChange={this.handleChange}
            id="outlined-adornment-weight"
            type="number"
            label={this.capitalizeString(this.props.placeholder)}
            margin="normal"
            variant="outlined"
            InputProps={{
              endAdornment: <InputAdornment position="end">Kg</InputAdornment>,
            }}
          />
          <Button className="result-btn" variant="contained" size="large" onClick={this.handleClick}>
            Get result
            <Icon>send</Icon>
          </Button>
        </div>
        <div className="result-box">
          <Confetti active={ this.state.confettiBoom } config={ config } className="confetti" />
          {this.state.isVisible ? <ResultBox resultValue={this.state.resultValue} /> : null}
        </div>
      </>  
    );
  }
}