import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles({
  card: {
    maxWidth: 350,
    minWidth: 250
  },
  media: {
    height: 170,
    backgroundSize: 'contain'
  },
  linkBtn: {
    background: 'linear-gradient(45deg, #5ebeff 30%, #a5d7ff 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
    color: 'white',
    height: 48,
    padding: '0 30px',
    margin: 8,
  },
  opacity: 0.5
});

const AdapterLink = React.forwardRef((props, ref) => (<Link innerRef={ref} {...props} />
  ));

export default function DeeperMetricBlock(props) {
  const handleClick = (e) => {
    e.preventDefault();
    console.log(e);
  }
  const classes = useStyles();
  return (
    <div>
      <Card className={classes.card} onClick={handleClick}>
        <CardMedia
          className={classes.media}
          image={props.img}
          title={props.name}
        />
        <CardActions>
          <Button className={classes.linkBtn} size="small" component={AdapterLink} to={props.link}>{props.name}</Button>
        </CardActions>
      </Card>
    </div>
  );
}