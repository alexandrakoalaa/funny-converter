import React from 'react';
import Paper from '@material-ui/core/Paper';

import { 
  FacebookShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  VKShareButton, 
  VKIcon,
  FacebookIcon,
  TelegramIcon,
  WhatsappIcon 
} from 'react-share';

export default class ResultBox extends React.Component {
  render() {
    return(
      <>
        <Paper className="result-box">
          <h1>{this.props.resultValue}</h1>
        </Paper>
        <div>
          <VKShareButton
            url={'https://material-ui.com/'}>
              <VKIcon size={36} round={true} />
          </VKShareButton>
          <FacebookShareButton
            url={'https://material-ui.com/'}>
              <FacebookIcon size={36} round={true} />
          </FacebookShareButton>
          <TelegramShareButton
            url={'https://material-ui.com/'}>
              <TelegramIcon size={36} round={true} />
          </TelegramShareButton>
          <WhatsappShareButton
            url={'https://material-ui.com/'}>
              <WhatsappIcon size={36} round={true} />
          </WhatsappShareButton>
        </div>
      </>
    )
  }
}