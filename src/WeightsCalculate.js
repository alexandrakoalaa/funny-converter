import React from 'react';

const kg = 1;
const centimeters = 1;
const noop = () => null

function genWeightFn (entity_weight) {
  return function weight(w) {
    return w / entity_weight;
  }
};

function genGrowthFn (entity_length) {
  return function length(l) {
    return l / entity_length;
  }
};

function genEntity (name, opts) {
  return {
    name,
    weight: opts.weight
      ? genWeightFn(opts.weight)
      : noop,
    length: opts.length
      ? genGrowthFn(opts.length)
      : noop
  }
}

export const elephant = genEntity('elephant', {
  weight: 6000 * kg
});

export const spaghetti = genEntity('spaghetti', {
  length: 25 * centimeters
});