import React from 'react';

 import DeeperMetricBlock from '../components/DeeperMetricBlock';

export default class MetricsPage extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      isLoaded: false,
      error: null,
      data: []
    }
  };

  componentDidMount() {
    const metricName = this.props.match.params.metric;
    fetch(`/units/${metricName}.json`)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          isLoaded: true,
          data: data
        })
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        })
      }
    )
  }

  render() {
    const { isLoaded, error, data } = this.state;

    const units = data.map((unit) => {
      return (
        <DeeperMetricBlock name={unit.name} link={unit.link} image={unit.img} key={unit.name} />
      );
    });
    if (error) {
      return (
        <h1>{error.message}</h1>
      )
    } else if (!isLoaded) {
      return (
        <h1>Loading...</h1>
      )
    } else {
      return(
        <div className="pages-style">
          <div className="flex-center margin-top">
            {units}
          </div>
        </div>
      );
    }
  }
}