import React from 'react';

import MetricBlock from '../components/MetricBlock';

export default class MainPage extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        isLoaded: false,
        data: [],
        error: null
      }
    }
    componentDidMount() {
      fetch('/metrics/metrics.json')
        .then(res => res.json())
        .then((data) => {
          const metrics = data.map(metric => {
            return <MetricBlock label={metric.label} link={metric.link} img={metric.img} key={metric.label} />
          })

          this.setState({
            isLoaded: true,
            data: metrics
          })
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          })
        })
    }

    render() {
      const {isLoaded, error, data} = this.state;
      if (error) {
        return (
          <h1>{error.message}</h1>
        )
      } else if (!isLoaded) {
        return (
          <h1>Loading...</h1>
        )
      } else {
        return (
          <div className="pages-style">
              <div className="flex-center margin-top">
                {data}
              </div>
          </div>
        );
      }
    }
}