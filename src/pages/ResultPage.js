import React from 'react';

import InputField from '../components/InputField';

export default function ResultPage (props) {

  const inputLabel = props.match.params.metric;

    return (
      <div className="pages-style">
        <div className="margin-top">
          <h1 className="text-center">Units page</h1>
          <InputField placeholder={inputLabel}  />
        </div>
      </div>
    )
  }