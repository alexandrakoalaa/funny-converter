import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './styles/App.css'

import TemporaryDrawer from './components/TemporaryDrawer';

import ResultPage from './pages/ResultPage';
import MetricsPage from './pages/MetricsPage';
import MainPage from './pages/MainPage';

export default class App extends React.Component {

  render() {
    return (
      <div className="app">
        <div className="bg-image"></div>
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={MainPage} />
            <Route path="/:metric" exact component={MetricsPage} />
            <Route path="/:metric/:unit" exact component={ResultPage} />
          </Switch>
        </BrowserRouter>
        <TemporaryDrawer transitionDuration={700}/>
      </div>
    );
  }
}